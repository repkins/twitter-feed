# Twitter Feed

Coding challenge

## Design

### Front-End

There is being used **NgRx platform** libraries to build scalable stateful applications. Directory structure is following at `app` dir:

* `actions` - defined action creators
* `reducers` - pure functions which takes previous app state and incoming action and apply it, creating new app state
* `selectors` - defined query selectors to use in components for listening state changes
* `effects` - defined reactive app behavior of actions by calling injected service methods and dispatching additional actions
* `services` - defined app services where calling HTTP requests functionality recides and subscribing to events in socket
* `models` - view models
* `components` - defined Angular sub-components

### Back-end

Sources defined in `src` dir, compiled files goes in `dist` dir. Server entry file is **server.ts**, where takes env variables, creates server instance and opens port. Then it imports main `App` instance where `express` and `socket.io` being initialized. Then loads routers for `express` for RESP API handling, and registers handlers for events in client socket connection, all of them defined in corresponding subdirs of main `app` dir - `routes` and `stream`. Tests defined in adjacent dir `spec` of `app` in sources root dir.

## Installation Notes

1. Do `npm i` in both frontend and backend directories of project root - `front` and `back` accordingly.
2. Do `npm run build` to compile TypeScript sources in `back` dir.
3. Make a copy of `.env.example` text file and rename a copy to `.env`, then open file with text editor and complete it with credentials for Twitter API.
4. Launch a **backend server** using `node dist/server.js` or simply `npm start`.
5. Launch a **frontend dev server** using `npm start`, or `ng serve` if angular-cli is installed globally too. 
6. Open in browser http://localhost:4200 to see app running.

## Tested on

* Microsoft Windows 10
* node.js v6.10.3
* npm v5.5.1
* Google Chrome v63.0.3239.132
