import * as chai from 'chai';
import chaiHttp = require('chai-http');

import * as sinon from 'sinon';
import * as sinonChai from "sinon-chai";

import * as Twitter from 'twitter';

import app from '../app/App';

chai.use(chaiHttp);
chai.use(sinonChai);
const expect = chai.expect;

describe('GET api/v1/tweets', () => {

  it('should use Twitter search api and response search results', async () => {
    const stubTwitterClient = sinon.createStubInstance<Twitter.Client>(Twitter);
    app.twitterApi(stubTwitterClient);

    const searchFixt = 'sample';
    const countFixt = "20";
    const tweetsFixt = [];

    stubTwitterClient.get.returns(Promise.resolve(tweetsFixt));

    const res = await chai.request(app.express).get('/api/v1/tweets?q='+searchFixt+'&count='+countFixt);

    expect(res.status).to.equal(200);
    expect(res).to.be.json;
    expect(res.body).to.eql(tweetsFixt);
    
    expect(stubTwitterClient.get).to.have.calledWith('search/tweets', { q: searchFixt, count: countFixt }, );
  });

  it('should response 400 on error', async () => {
    const stubTwitterClient = sinon.createStubInstance<Twitter.Client>(Twitter);
    app.twitterApi(stubTwitterClient);

    stubTwitterClient.get.returns(Promise.reject([]));

    try {
      await chai.request(app.express).get('/api/v1/tweets');
    } catch (errRes) {
      expect(errRes.status).to.equal(400);
    }
  });
});