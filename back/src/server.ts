import * as http from 'http';
import * as debug from 'debug';
import * as Twitter from 'twitter';
import * as socketio from 'socket.io';
import * as dotenv from "dotenv";

import App from './app/App';

debug('ts-express:server');

dotenv.config();
process.on('unhandledRejection', err => {
  console.error(err);
  
  throw err;
});

const twitterApiClient = new Twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
  access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
});
App.twitterApi(twitterApiClient);

const port = normalizePort(process.env.PORT || 3000);
App.express.set('port', port);

const server = http.createServer(App.express);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

// Socket.IO
const io = socketio(server);
App.socketIO(io);

function normalizePort(val: number|string) {
  let port: number = (typeof val === 'string') ? parseInt(val, 10) : val;

  if (isNaN(port)) {
    return val;
  } else if (port >= 0) {
    return port;
  } else {
    return false;
  }
}

function onError(error: NodeJS.ErrnoException) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port;

  switch(error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
    break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
    break;
    default:
      throw error;
  }
}

function onListening() {
  let addr = server.address();
  let bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`;

  debug(`Listening on ${bind}`);
}
