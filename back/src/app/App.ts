import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';

import { Client as TwitterClient } from "twitter";

import TweetRouter from './routes/TweetRouter';
import { TrackStream } from "./stream/track";

// Creates and configures an ExpressJS web server.
class App {

  // ref to Express instance
  public express: express.Application;

  private _twitterApiClient: TwitterClient;

  private _io: SocketIO.Server;

  // Run configuration methods on the Express instance.
  constructor() {
    this.express = express();
    this._middleware();
    this._routes();
  }

  // Configure Twitter API
  twitterApi(twitterApiClient: TwitterClient) {
    this._twitterApiClient = twitterApiClient;

    this._twitterRouting();
    this._verifyTwitterApi();
  }

  // Configure SocketIO
  socketIO(io: SocketIO.Server) {
    this._io = io;

    this._registerSocketHandlers();
  }

  // Configure Express middleware.
  private _middleware() {
    this.express.use(logger('dev'));
    this.express.use(cors());
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }

  // Configure API endpoints.
  private _routes() {
    this.express.use('/api/v1/tweets', TweetRouter.router);
  }

  // Configure Routers for Twitter API usage
  private _twitterRouting() {
    TweetRouter.configureTwitterApi(this._twitterApiClient);
  }

  private async _verifyTwitterApi() {
    // Test Auth
    try {
      await this._twitterApiClient.get('account/verify_credentials', {});

    } catch (errs) {
      if (errs && errs instanceof Array) {
        const err = errs.shift();
        
        if (err && err.code && (err.code === 32 || err.code === 215)) {
          throw new Error('Failed to authenticate Twitter API in this environment');
        } else {
          console.error(errs);
        }
      } else {
        console.error(errs);
      }
    }
  }

  private _registerSocketHandlers() {
    this._io.on('connection', socket => {
      console.log('socket.io client connected');
    
      socket.on('disconnect', () => {
        console.log('socket.io client disconnected');
      });

      const trackStream = new TrackStream(socket, this._twitterApiClient);
      trackStream.registerHandlers();
    });
  }
}

export default new App();
