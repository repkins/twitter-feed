import { 
  Client as TwitterClient, 
  Stream as TwitterStream 
} from "twitter";

export class TrackStream {
  
  private _twStream: TwitterStream;

  constructor(private _socket: SocketIO.Socket, private _twitterClient: TwitterClient) { }

  private _trackStream(data: any) {
    this._stopTrack();

    const trackBy = data.track;

    this._twStream = this._twitterClient.stream('statuses/filter', { track: trackBy });
    this._twStream.on('data', (event) => {
      this._socket.emit('status', event);
    });
     
    this._twStream.on('error', (error) => {
      if (error.source) {
        this._socket.emit('err', error);
      } else {
        console.error(error);
      }
    });
    
    this._socket.on('disconnect', () => {
      this._stopTrack();
    });
  }

  private _stopTrack() {
    if (this._twStream) {
      this._twStream.destroy();
    }
  }

  registerHandlers() {
    this._socket.on('track', data => this._trackStream(data));
  }

}
