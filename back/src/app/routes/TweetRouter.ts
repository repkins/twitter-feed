import { Router, Request, Response, NextFunction } from 'express';
import { Client as TwitterClient } from 'twitter';

export class TweetRouter {
  router: Router;

  private _twitterClient: TwitterClient;

  /**
   * Initialize the TweetRouter
   */
  constructor() {
    this.router = Router();
    this.init();
  }

  /**
   * GET Tweets.
   */
  public async getTweets(req: Request, res: Response, next: NextFunction) {
    const search: string = req.query.q || '';
    const count: number = req.query.count || 10;

    try {
      let tweets = await this._twitterClient.get('search/tweets', { q: search, count } );

      res.send(tweets);
    } catch (errs) {
      console.error(errs);
      
      res.status(400);
      res.send(errs);
    }
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.get('/', (req, res, next) => this.getTweets(req, res, next));
  }

  /**
   * Configures Twitter API for this router
   * 
   * @param twitterClient Twitter API client instance
   */
  configureTwitterApi(twitterClient: TwitterClient) {
    this._twitterClient = twitterClient;
  }
}

// Create the TweetRouter, and export its configured Express.Router
const tweetRoutes = new TweetRouter();
tweetRoutes.init();

export default tweetRoutes;
