declare module 'twitter' {
  import { EventEmitter } from "events";

  namespace Twitter {
    export interface Client {

      get(path: string, params: any): Promise<any>;
      post(path: string, params: any): Promise<any>;
      stream(path: string, params: any): Stream;
    }

    export interface Stream extends EventEmitter {
      destroy(): void;
    }
  }

  interface TwitterAPI {
    new (options: any): Twitter.Client;
  }

  const Twitter: TwitterAPI;

  export = Twitter;
}
