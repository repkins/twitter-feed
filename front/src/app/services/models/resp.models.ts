import { ListPost } from 'app/models/list-post.model';

export interface RespListTweets {
  statuses: ListPost[];
}
