import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import * as socketIo from 'socket.io-client';

import { RespListTweets } from './models/resp.models';
import { ListPost } from '../models/list-post.model';

@Injectable()
export class PostsService {

  private readonly _host = 'http://localhost:3000';
  private _socket = socketIo(this._host);

  statusNotifs$ = Observable
    .fromEvent<ListPost>(this._socket, 'status')
    .merge(Observable
      .fromEvent(this._socket, 'err').map(({ source }) => { throw new Error(source); })
    );

  constructor(
    private _http: HttpClient
  ) { }

  searchPosts(query: string) {
    const queryOpts = {
      params: new HttpParams().set('q', query).set('count', '10')
    };

    return this._http.get<RespListTweets>(`${this._host}/api/v1/tweets`, queryOpts)
      .catch((resErr) => {
        if (resErr && resErr.error && resErr.error instanceof Array) {
          const err = (resErr.error as any[]).shift();

          if (err.code === 25) {
            throw new Error('Please give a query');
          } else {
            throw new Error(err.message);
          }

        } else {
          throw new Error(resErr.message);
        }
      })
      .map((resp: RespListTweets) => resp.statuses);
  }

  trackPosts(query: string) {
    this._socket.emit('track', { track: query });
  }
}
