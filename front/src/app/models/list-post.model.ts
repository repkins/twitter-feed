export class ListPost {
  constructor(
    public id: string,
    public text: string,
    public user: PostUser
  ) {}
}

export class PostUser {
  constructor(
    public id: string,
    public name: string,
    public description: string,
    public profile_image_url: string
  ) {}
}
