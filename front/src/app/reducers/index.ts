import { ActionReducer, ActionReducerMap } from '@ngrx/store';

import * as fromPosts from './posts-list/posts.reducer';

export interface State {
  posts: fromPosts.State;
}

export const reducers: ActionReducerMap<State> = {
  posts: fromPosts.reducer
};

export const metaReducers: any[] = [];

export const getPosts = (state: State) => state.posts;

export {
  fromPosts
};
