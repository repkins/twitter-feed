import { fromPostsActions } from '../../actions';
import { ListPost } from '../../models/list-post.model';

import Posts = fromPostsActions.Types;

export class State {
  data: ListPost[] = null;

  isFetching = false;
  fetchErrMsg: string = null;

  constructor(prevState: Partial<State> = {}) {
    Object.assign(this, prevState);
  }
}

const initState = new State();

type Actions = fromPostsActions.Actions;
export function reducer(state: Readonly<State> = initState, action: Actions) {
  const newState = new State(state);

  /** data */
  switch (action.type) {
    case Posts.SearchPostsSuccessType:
      newState.data = action.payload;
    break;

    case Posts.ReceiveNewPostType:
      newState.data = [ action.payload, ...state.data ];
      newState.data.pop();
    break;
  }

  /** isFetching */
  switch (action.type) {
    case Posts.SearchPostsType:
      newState.isFetching = true;
    break;

    case Posts.SearchPostsSuccessType:
    case Posts.SearchPostsFailedType:
      newState.isFetching = false;
    break;
  }

  /** fetchErrMsg */
  switch (action.type) {
    case Posts.SearchPostsType:
      newState.fetchErrMsg = null;
    break;

    case Posts.SearchPostsFailedType:
      newState.fetchErrMsg = action.payload;
    break;
  }


  return newState;
}

export const getData = (state: State) => state.data;
export const getIsFetching = (state: State) => state.isFetching;
export const getFetchErrMsg = (state: State) => state.fetchErrMsg;
