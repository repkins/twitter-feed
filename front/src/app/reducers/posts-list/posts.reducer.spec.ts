import { fromPostsActions } from '../../actions';
import { State, reducer } from './posts.reducer';

import { ListPost } from '../../models/list-post.model';

describe('PostsReducer', () => {

  // Posts Actions

  it('on action "SearchPosts" should set #isFetching to "true" and set #fetchErrMsg to "null"', () => {
    const prevState = new State({
      isFetching: false,
      fetchErrMsg: 'some messags'
    });
    const expState = new State({
      isFetching: true,
      fetchErrMsg: null
    });

    const action = new fromPostsActions.SearchPosts(undefined);
    const state = new State(prevState);
    const result = reducer(state, action);

    expect(state).toEqual(prevState);
    expect(result).toEqual(expState);
  });
  it('on action "SearchPostsSuccess" should assign #data and set #isFetching to "false"', () => {
    const fixt = {
      data: [ {} ] as ListPost[]
    };

    const prevState = new State({
      data: null,
      isFetching: true
    });
    const expState = new State({
      data: fixt.data,
      isFetching: false
    });

    const payload = fixt.data;
    const action = new fromPostsActions.SearchPostsSuccess(payload);
    const state = new State(prevState);
    const result = reducer(state, action);

    expect(state).toEqual(prevState);
    expect(result).toEqual(expState);
  });
  it('on action "SearchPostsFailed" should set #isFetching to "false" and assign #fetchErrMsg', () => {
    const prevState = new State({
      isFetching: true,
      fetchErrMsg: null
    });
    const expState = new State({
      isFetching: false,
      fetchErrMsg: 'some message'
    });

    const action = new fromPostsActions.SearchPostsFailed('some message');
    const state = new State(prevState);
    const result = reducer(state, action);

    expect(state).toEqual(prevState);
    expect(result).toEqual(expState);
  });

  it('on action "ReceiveNewPost" should assign #data with new item', () => {
    const fixt = {
      newData: {} as ListPost
    };

    const prevState = new State({
      data: [ null ] as ListPost[],
    });
    const expState = new State({
      data: [ fixt.newData ],
    });

    const payload = fixt.newData;
    const action = new fromPostsActions.ReceiveNewPost(payload);
    const state = new State(prevState);
    const result = reducer(state, action);

    expect(state).toEqual(prevState);
    expect(result).toEqual(expState);
    expect(prevState).not.toEqual(expState);
  });
});
