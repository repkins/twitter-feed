import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';

import { from } from 'rxjs/observable/from';
import { of } from 'rxjs/observable/of';
import { merge } from 'rxjs/observable/merge';

import { PostsService } from '../services/posts.service';
import { fromPostsActions } from '../actions';

@Injectable()
export class PostsEffects {

  // Fetching

  @Effect() searchPosts$ = this._actions$
    .ofType(fromPostsActions.Types.SearchPostsType)
    .map((action: fromPostsActions.SearchPosts) => action.payload)
    .switchMap((query) => {

      return from(this._service.searchPosts(query))
        .switchMap((val) => {
          this._service.trackPosts(query);

          return of(new fromPostsActions.SearchPostsSuccess(val));
        })
        .catch((err: Error) => of(new fromPostsActions.SearchPostsFailed(err.message)));
    });

  @Effect() searchPostsSuccess$ = this._actions$
    .ofType(fromPostsActions.Types.SearchPostsSuccessType)
    .map((action: fromPostsActions.SearchPostsSuccess) => action.payload)
    .switchMap(() => {

      const statusNotifs$ = this._service.statusNotifs$;

      return statusNotifs$
        .map(post => new fromPostsActions.ReceiveNewPost(post))
        .catch(err => of(new fromPostsActions.SearchPostsFailed(err.message)));
    });

  constructor(
    private _actions$: Actions,
    private _service: PostsService
  ) { }
}
