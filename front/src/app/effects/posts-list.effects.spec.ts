import { Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { PostsService } from '../services/posts.service';

import { ListPost } from '../models/list-post.model';
import { fromPostsActions } from '../actions';

import { PostsEffects } from './posts-list.effects';

describe('Contracts: ContractMembersListEffects', () => {
  let actionsStub: jasmine.SpyObj<Actions>;
  let fetchStub: jasmine.SpyObj<Partial<PostsService>>;

  let effects: PostsEffects;

  let actions$: Subject<Action>;
  let statusNotifs$: Subject<ListPost>;

  beforeEach(() => {
    actions$ = new Subject<Action>();
    statusNotifs$ = new Subject<ListPost>();

    actionsStub = jasmine.createSpyObj([ 'ofType' ] as (keyof typeof actionsStub)[]);
    actionsStub.ofType.and.callFake((...keys: string[]) => {
      return actions$.filter(val => keys.includes(val.type));
    });
    fetchStub = {
      statusNotifs$: statusNotifs$,
      searchPosts: jasmine.createSpy('searchPosts'),
      trackPosts: jasmine.createSpy('trackPosts'),
    } as { [P in keyof typeof fetchStub]: any; };

    effects = new PostsEffects(actionsStub, fetchStub as jasmine.SpyObj<PostsService>);
  });

  describe('#searchPosts', () => {
    it('should make "searchPosts" request and emit #searchPostsSuccess action on success', () => {
      const fixtIn = 'sample';
      const fixtOut = [] as ListPost[];

      const subscrStub = jasmine.createSpy('subscribe fn');
      effects.searchPosts$.subscribe(subscrStub);

      fetchStub.searchPosts.and.returnValue(Observable.of(fixtOut));

      const payload = fixtIn;
      actions$.next(new fromPostsActions.SearchPosts(payload));

      expect(fetchStub.searchPosts).toHaveBeenCalledWith(fixtIn);
      expect(subscrStub).toHaveBeenCalledWith(new fromPostsActions.SearchPostsSuccess(fixtOut));

      expect(fetchStub.trackPosts).toHaveBeenCalledWith(fixtIn);
    });
    it('should make "searchPosts" request and emit #searchPostsFailed action on error', () => {
      const fixtIn = 'sample';
      const fixtOut =  {
        message: 'msg'
      };

      const subscrStub = jasmine.createSpy('subscribe fn');
      effects.searchPosts$.subscribe(subscrStub);

      fetchStub.searchPosts.and.returnValue(Observable.throw(fixtOut));

      const payload = fixtIn;
      actions$.next(new fromPostsActions.SearchPosts(payload));

      expect(fetchStub.searchPosts).toHaveBeenCalledWith(fixtIn);
      expect(subscrStub).toHaveBeenCalledWith(new fromPostsActions.SearchPostsFailed(fixtOut.message));
    });
  });

  describe('#searchPostsSuccess', () => {
    it('should emit #receiveNewPost action on posts status notifications', () => {
      const fixtIn = [ {} ] as ListPost[];

      const subscrStub = jasmine.createSpy('subscribe-fn');
      effects.searchPostsSuccess$.subscribe((...args: any[]) => subscrStub(...args));

      const payload = fixtIn;
      actions$.next(new fromPostsActions.SearchPostsSuccess(payload));

      const notif = {} as ListPost;
      statusNotifs$.next(notif);

      expect(subscrStub).toHaveBeenCalledWith(new fromPostsActions.ReceiveNewPost(notif));
    });
  });

});
