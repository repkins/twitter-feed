import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { fromPostsListSelectors } from './selectors';
import { ListPost } from 'app/models/list-post.model';
import { fromPostsActions } from './actions';

import { State } from './reducers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  postsList$: Observable<ListPost[]>;
  isFetching$: Observable<boolean>;
  hasFetchErr$: Observable<boolean>;
  fetchErrMsg$: Observable<string>;

  hasFetchErrSubscr: Subscription;
  postsListSubscr: Subscription;

  constructor(private store: Store<State>, private _snackBar: MatSnackBar) {
    this.postsList$ = store.let(fromPostsListSelectors.getPosts);
    this.isFetching$ = store.let(fromPostsListSelectors.getIsFetching);
    this.hasFetchErr$ = store.let(fromPostsListSelectors.getHasFetchErr);
    this.fetchErrMsg$ = store.let(fromPostsListSelectors.getFetchErrMsg);
  }

  ngOnInit() {
    this.hasFetchErrSubscr = this.hasFetchErr$.filter(value => value).subscribe(async () => {
      const errMsg = await this.fetchErrMsg$.first().toPromise();

      this._snackBar.open(errMsg, '', {
        duration: 5000
      });
    });

    this.postsListSubscr = this.postsList$.filter(value => value && value.length === 0).subscribe(() => {
      this._snackBar.open('Nothing to show', '', {
        duration: 5000
      });
    });
  }

  ngOnDestroy() {
    this.hasFetchErrSubscr.unsubscribe();
    this.postsListSubscr.unsubscribe();
  }

  // Event Handlers

  onSearchPosts(query: string) {
    this.store.dispatch(new fromPostsActions.SearchPosts(query));
  }

}
