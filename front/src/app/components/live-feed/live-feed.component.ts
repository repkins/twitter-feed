import { Component, OnInit, Input } from '@angular/core';
import { ListPost } from 'app/models/list-post.model';

@Component({
  selector: 'app-live-feed',
  templateUrl: './live-feed.component.html',
  styleUrls: ['./live-feed.component.scss']
})
export class LiveFeedComponent implements OnInit {

  @Input() posts: ListPost[] = [];

  constructor() { }

  ngOnInit() {
  }

}
