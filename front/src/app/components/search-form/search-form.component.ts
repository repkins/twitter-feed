import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  term = '';

  @Output() search = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onSearchClick() {
    const term = this.term;

    this.search.emit(term);
  }
}
