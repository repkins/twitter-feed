import * as fromPostsListData from './posts-list/data.selectors';

const fromPostsListSelectors = {
  ...fromPostsListData,
};

export { fromPostsListSelectors };
