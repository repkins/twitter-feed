import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { ListPost } from '../../models/list-post.model';

import { getPosts, getIsFetching, getHasFetchErr, getFetchErrMsg } from './data.selectors';

import * as fromRoot from '../../reducers';
import { fromPosts } from '../../reducers';

describe('PostsListData Selectors', () => {
  let storeStub: Store<fromRoot.State>;
  let state: Pick<fromRoot.State, 'posts'>;

  const state$ = new Subject<typeof state>();

  beforeEach(() => {
    storeStub = state$.asObservable() as typeof storeStub;
    storeStub.select = function (this: typeof storeStub, mapFn: (state: fromRoot.State) => any) {
      const mapped$ = this.map(mapFn).distinctUntilChanged() as typeof storeStub;

      mapped$.select = storeStub.select;
      return mapped$;
    };

    state = {
      posts: new fromPosts.State(),
    };
  });

  describe('#getPosts', () => {
    let query$: Observable<ListPost[]>;

    beforeEach(() => {
      query$ = getPosts(storeStub);
    });

    it('on posts "data" null should receive null', () => {
      let subscrStub = jasmine.createSpy('subscribe-fn');
      query$.take(2).subscribe((...args: any[]) => subscrStub(...args));

      state.posts.data = null;
      state$.next(state);

      const expectedVal: ListPost[] = null;
      expect(subscrStub).toHaveBeenCalledWith(expectedVal);

      subscrStub = jasmine.createSpy('subscribe-fn');
      state.posts = { ...state.posts };
      state$.next(state);
      expect(subscrStub).not.toHaveBeenCalled();
    });
    it('on posts "data" non-null array should receive passed data', () => {
      const fixtData = [ { } ] as ListPost[];
      let subscrStub = jasmine.createSpy('subscribe-fn');
      query$.take(2).subscribe((...args: any[]) => subscrStub(...args));

      state.posts.data = fixtData;
      state$.next(state);

      const expectedVal: ListPost[] = fixtData;
      expect(subscrStub).toHaveBeenCalledWith(expectedVal);

      subscrStub = jasmine.createSpy('subscribe-fn');
      state.posts = { ...state.posts };
      state$.next(state);
      expect(subscrStub).not.toHaveBeenCalled();
    });
  });

  describe('#getIsFetching', () => {
    let query$: Observable<boolean>;

    beforeEach(() => {
      query$ = getIsFetching(storeStub);
    });

    it('on posts "isFetching" true should receive true', () => {
      let subscrStub = jasmine.createSpy('subscribe-fn');
      query$.take(2).subscribe((...args: any[]) => subscrStub(...args));

      state.posts.isFetching = true;
      state$.next(state);

      const expectedVal = true;
      expect(subscrStub).toHaveBeenCalledWith(expectedVal);

      subscrStub = jasmine.createSpy('subscribe-fn');
      state.posts = { ...state.posts };
      state$.next(state);
      expect(subscrStub).not.toHaveBeenCalled();
    });
    it('on posts "isFetching" false should receive false', () => {
      let subscrStub = jasmine.createSpy('subscribe-fn');
      query$.take(2).subscribe((...args: any[]) => subscrStub(...args));

      state.posts.isFetching = false;
      state$.next(state);

      const expectedVal = false;
      expect(subscrStub).toHaveBeenCalledWith(expectedVal);

      subscrStub = jasmine.createSpy('subscribe-fn');
      state.posts = { ...state.posts };
      state$.next(state);
      expect(subscrStub).not.toHaveBeenCalled();
    });
  });

  describe('#getHasFetchErr', () => {
    let query$: Observable<boolean>;

    beforeEach(() => {
      query$ = getHasFetchErr(storeStub);
    });

    it('on posts "fetchErrMsg" null should receive false', () => {
      let subscrStub = jasmine.createSpy('subscribe-fn');
      query$.take(2).subscribe((...args: any[]) => subscrStub(...args));

      state.posts.fetchErrMsg = null;
      state$.next(state);

      const expectedVal = false;
      expect(subscrStub).toHaveBeenCalledWith(expectedVal);

      subscrStub = jasmine.createSpy('subscribe-fn');
      state.posts = { ...state.posts };
      state$.next(state);
      expect(subscrStub).not.toHaveBeenCalled();
    });
    it('on posts "fetchErrMsg" non-null should receive true', () => {
      let subscrStub = jasmine.createSpy('subscribe-fn');
      query$.take(2).subscribe((...args: any[]) => subscrStub(...args));

      state.posts.fetchErrMsg = 'some';
      state$.next(state);

      const expectedVal = true;
      expect(subscrStub).toHaveBeenCalledWith(expectedVal);

      subscrStub = jasmine.createSpy('subscribe-fn');
      state.posts = { ...state.posts };
      state$.next(state);
      expect(subscrStub).not.toHaveBeenCalled();
    });
  });

  describe('#getFetchErrMsg', () => {
    let query$: Observable<string>;

    beforeEach(() => {
      query$ = getFetchErrMsg(storeStub);
    });

    it('on posts "fetchErrMsg" null should receive null', () => {
      let subscrStub = jasmine.createSpy('subscribe-fn');
      query$.take(2).subscribe((...args: any[]) => subscrStub(...args));

      state.posts.fetchErrMsg = null;
      state$.next(state);

      const expectedVal: string = null;
      expect(subscrStub).toHaveBeenCalledWith(expectedVal);

      subscrStub = jasmine.createSpy('subscribe-fn');
      state.posts = { ...state.posts };
      state$.next(state);
      expect(subscrStub).not.toHaveBeenCalled();
    });
    it('on posts "fetchErrMsg" non-null should receive passed message', () => {
      const fixt = 'some msg';
      let subscrStub = jasmine.createSpy('subscribe-fn');
      query$.take(2).subscribe((...args: any[]) => subscrStub(...args));

      state.posts.fetchErrMsg = fixt;
      state$.next(state);

      const expectedVal: string = fixt;
      expect(subscrStub).toHaveBeenCalledWith(expectedVal);

      subscrStub = jasmine.createSpy('subscribe-fn');
      state.posts = { ...state.posts };
      state$.next(state);
      expect(subscrStub).not.toHaveBeenCalled();
    });
  });
});
