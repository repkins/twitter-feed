import { Store } from '@ngrx/store';

import * as fromRoot from '../../reducers';
import { fromPosts } from '../../reducers';

export const getPosts = (state$: Store<fromRoot.State>) => {
  return state$
    .select(fromRoot.getPosts).select(fromPosts.getData);
};

export const getIsFetching = (state$: Store<fromRoot.State>) => {
  return state$
    .select(fromRoot.getPosts).select(fromPosts.getIsFetching);
};
export const getHasFetchErr = (state$: Store<fromRoot.State>) => {
  return state$
    .select(fromRoot.getPosts).select(fromPosts.getFetchErrMsg)
    .map(val => !!val);
};
export const getFetchErrMsg = (state$: Store<fromRoot.State>) => {
  return state$
    .select(fromRoot.getPosts).select(fromPosts.getFetchErrMsg);
};
