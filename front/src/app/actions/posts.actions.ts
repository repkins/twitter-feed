import { Action } from '@ngrx/store';

import { ListPost } from '../models/list-post.model';

export namespace Types {
  export const SearchPostsType = '[Posts] Search Posts';
  export const SearchPostsSuccessType = '[Posts] Search Posts Success';
  export const SearchPostsFailedType = '[Posts] Search Posts Failed';

  export const ReceiveNewPostType = '[Posts] Receive New Post';
}

export class SearchPosts implements Action {
  readonly type = Types.SearchPostsType;

  constructor(public payload: string) {}
}
export class SearchPostsSuccess implements Action {
  readonly type = Types.SearchPostsSuccessType;

  constructor(public payload: ListPost[]) { }
}
export class SearchPostsFailed implements Action {
  readonly type = Types.SearchPostsFailedType;

  constructor(public payload: string) { }
}

export class ReceiveNewPost implements Action {
  readonly type = Types.ReceiveNewPostType;

  constructor(public payload: ListPost) { }
}

export type Actions
  = SearchPosts
  | SearchPostsSuccess
  | SearchPostsFailed
  | ReceiveNewPost;
