import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { MatButtonModule, MatInputModule, MatCardModule, MatSnackBarModule, MatProgressBarModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { environment } from 'environments/environment';

import { reducers, metaReducers } from './reducers';
import { PostsEffects } from './effects/posts-list.effects';

import { PostsService } from 'app/services/posts.service';
import { AppComponent } from './app.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { LiveFeedComponent } from './components/live-feed/live-feed.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchFormComponent,
    LiveFeedComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NoopAnimationsModule,
    FormsModule,

    StoreModule.forRoot(reducers, {
      metaReducers
    }),
    EffectsModule.forRoot([
      PostsEffects,
    ]),

    FlexLayoutModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatSnackBarModule,
    MatProgressBarModule,

    !environment.production ? StoreDevtoolsModule.instrument({
      maxAge: 50
    }) : [],
  ],
  providers: [ PostsService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
